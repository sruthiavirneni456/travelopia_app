import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { FirstPageComponent } from './first-page/first-page.component';
import { SecondPageComponent } from './second-page/second-page.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterLink, RouterModule, Routes} from '@angular/router';
import {FirstPageService} from './first-page/services/first-page.service';
import {HttpClientModule} from '@angular/common/http';

const routes:Routes=[
  {
    path:"",
    component:FirstPageComponent
  },
  {
    path:"firstPage",
    component:FirstPageComponent,
  },
  {
    path:"secondPage",
    component:SecondPageComponent,
  }
]

@NgModule({
  declarations: [
    AppComponent,
    FirstPageComponent,
    SecondPageComponent,
  ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterLink,
        RouterModule.forRoot(routes),
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
