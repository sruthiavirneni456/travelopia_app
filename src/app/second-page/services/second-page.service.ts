import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root',
})
export class SecondPageService {
  constructor(private httpClientService: HttpClient) {
  }
  private baseUrl = 'http://localhost:3000'
  listData(body:any): Observable<any> {
    return this.httpClientService.post(
      `${this.baseUrl}/list`,
      body,
      {}
    );
  }
}
