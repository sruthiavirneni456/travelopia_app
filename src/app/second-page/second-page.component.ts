import { Component } from '@angular/core';
import {FirstPageService} from '../first-page/services/first-page.service';

@Component({
  selector: 'app-second-page',
  templateUrl: './second-page.component.html',
  styleUrls: ['./second-page.component.css']
})
export class SecondPageComponent {
  constructor(private firsPageService:FirstPageService) {
  }
  travelopiaList = this.firsPageService.finalFormResultData;
}
