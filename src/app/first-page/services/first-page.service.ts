import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root',
})
export class FirstPageService {
  constructor(private httpClientService: HttpClient) {
  }
  finalFormResultData:any;
  private baseUrl = 'http://localhost:3000'
  create(body:any): Observable<any> {
    return this.httpClientService.post(
      `${this.baseUrl}/create`,
      body,
      {}
    );
  }

  listData(): Observable<any> {
    return this.httpClientService.get(
      `${this.baseUrl}/list`,
      {}
    );
  }
}
