import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FirstPageService} from './services/first-page.service';
import {TravelopiaSchema} from './constants/first-page.constants';
import {HttpErrorResponse} from '@angular/common/http';
import { Router} from '@angular/router';

@Component({
  selector: 'app-first-page',
  templateUrl: './first-page.component.html',
  styleUrls: ['./first-page.component.css']
})
export class FirstPageComponent implements OnInit{
  formGroup!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private firstPageService:FirstPageService,
    private router:Router
  ) {

  }

  ngOnInit() {
    this.formGroup = this.fb.group({
      name: this.fb.control(null,[]),
      email:this.fb.control(null,[]),
      location: this.fb.control(null,[]),
      travellers: this.fb.control(null,[]),
      budget: this.fb.control(null,[])
    });
  }
  onSubmit() {
    if (this.formGroup.valid) {
      let travelopiaData:TravelopiaSchema={
        name: this.formGroup?.value?.name,
        emailAddress:this.formGroup?.value?.email,
        location:this.formGroup?.value?.location,
        travellers:this.formGroup?.value?.travellers,
        budget:this.formGroup?.value?.budget,

      }
      this.firstPageService?.create(travelopiaData).subscribe(
        {
          next:(results)=>{
            console.log("success: Data saved");
          },
          error:(errorResponse: HttpErrorResponse)=>{
            console.log("error response");
          }

        }
      )
      console.log(this.formGroup.value);
      this.formGroup.reset();
    }
  }
  retrieveFormInfo(){
    this.firstPageService?.listData().subscribe({
      next:(results)=>{
        if(results){
          this.firstPageService.finalFormResultData= results;
          this.router.navigateByUrl(`/secondPage`).then();
        }
        console.log("results",results);
      },
      error:(errorResponse: HttpErrorResponse)=>{
        console.log("error response");
      }
    })
  }
}
