export class TravelopiaSchema{
  name?: string;
  emailAddress?:string;
  location?:string;
  budget?:number;
  travellers?:number;
}
